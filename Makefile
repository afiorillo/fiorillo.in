
install:
	pipenv --bare install --clear --skip-lock --dev

lint:
	pipenv run flake8 .

unit_test:
	pipenv run nosetests -sv

integration_test:
	echo OK

deploy_test: install
	pipenv lock -r > requirements.txt
	gcloud app deploy --quiet --no-promote --version=$(GAE_VERSION) --project=$(GAE_PROJECT_ID) app.yaml

teardown_test:
	gcloud app versions delete --project=$(GAE_PROJECT_ID) --quiet $(GAE_VERSION)

deploy_prod: install
	pipenv lock -r > requirements.txt
	gcloud app deploy --quiet --promote --version=$(GAE_VERSION) --project=$(GAE_PROJECT_ID) app.yaml

webpack_build:
	# node_template
	cd fiorillo_in/node_template/static; \
		npm install; \
		npm run build;
	
dev_server:
	export FLASK_APP=fiorillo_in.app; \
	export FLASK_ENV=development; \
	export ENVIRONMENT=dev; \
	pipenv run flask run
