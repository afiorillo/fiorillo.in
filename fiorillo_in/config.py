import os

__all__ = [
    'ProductionConfig', 'DevelopmentConfig', 'get_config', 'current_config'
]


class BaseConfig:
    DEBUG = True


class ProductionConfig(BaseConfig):
    DEBUG = False


class DevelopmentConfig(BaseConfig):
    pass


__current_config = os.environ.get('ENVIRONMENT', 'dev')
__config_map = {'prod': ProductionConfig, 'dev': DevelopmentConfig}


def get_config(name=__current_config):
    return __config_map.get(name, DevelopmentConfig)


current_config = get_config()
