from flask import redirect


def healthcheck():
    return 'all good', 200


def favicon():
    return b'', 200


def send_to_blog():
    return redirect('https://fiorillo.eu', code=302)
