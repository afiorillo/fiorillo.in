// bohr model - all spheres of various R
AFRAME.registerComponent('bohr-electron', {
  schema: {
    // orbital settings
    orbitalRadius: {default: 1},
    orbitalPhase: {default: 0}, // given in degrees
    orbitsPerSec: {default: 1},
    thetaX: {default: 0}, // rotation about X-axis, degrees given
    thetaY: {default: 0}, // rotation about Y-axis, degrees given
    // point-object settings
    pointRadius: {default: 0.05},
    pointColor: {default: "#0000FF"},
  },

  init: function() {
    // convert degrees to radians
    this.data.orbitalPhase *= Math.PI/180;
    this.data.thetaX *= Math.PI/180;
    this.data.thetaY *= Math.PI/180;

    // construct the mesh
    var data = this.data;
    var el = this.el;
    this.geometry = new THREE.SphereBufferGeometry(data.pointRadius);
    this.material = new THREE.MeshLambertMaterial({color: data.pointColor});
    this.mesh = new THREE.Mesh(this.geometry, this.material);
    el.setObject3D('mesh', this.mesh);
  },

  getPosition: function(t) {
    // circular orbit rotated about X- and Y-axes
    let thetaX = this.data.thetaX;
    let thetaY = this.data.thetaY;
    let phi = this.data.orbitalPhase;
    let R = this.data.orbitalRadius;
    let f = this.data.orbitsPerSec;
    return {
      x: R * (
        Math.cos(thetaY) * Math.cos(2*Math.PI*f*t + phi)
      ),
      y: R * (
        Math.sin(thetaX)*Math.sin(thetaY)*Math.cos(2*Math.PI*f*t + phi) + 
        Math.cos(thetaX)*Math.sin(2*Math.PI*f*t + phi)
      ),
      z: R * (
        -Math.cos(thetaX)*Math.sin(thetaY)*Math.cos(2*Math.PI*f*t + phi) +
        Math.sin(thetaX)*Math.sin(2*Math.PI*f*t + phi)
      ),
    }
  },

  update: function (oldData) {
    // data has changed
  },

  tick: function (t, dt) {
    // calculate
    t = t / 1000; // seconds
    dt = dt / 1000; // seconds
    let pos = this.getPosition(t);
    // update mesh
    var mesh = this.el.getObject3D('mesh');
    mesh.position.x = pos.x;
    mesh.position.y = pos.y;
    mesh.position.z = pos.z;
  }
});

AFRAME.registerComponent('bohr-electron-track', {
  schema: {
    // orbital settings
    orbitalRadius: {default: 1},
    orbitsPerSec: {default: 1},
    thetaX: {default: 0}, // rotation about X-axis, degrees given
    thetaY: {default: 0}, // rotation about Y-axis, degrees given
    // point-object settings
    pointRadius: {default: 0.05},
    pointColor: {default: "#0000FF"},
    // track opacity settings
    trackOpacity: {default: 0.15},
  },

  init: function() {
    // convert degrees to radians
    this.data.thetaX *= Math.PI/180;
    this.data.thetaY *= Math.PI/180;

    // construct the mesh
    var data = this.data;
    var el = this.el;
    let radialSegments = 24;
    let tubularSegments = 30;
    this.geometry = new THREE.TorusBufferGeometry(
      data.orbitalRadius, data.pointRadius, radialSegments, tubularSegments);
    this.material = new THREE.MeshLambertMaterial({
      color: data.pointColor,
      opacity: data.trackOpacity,
      transparent: true,
    });
    this.mesh = new THREE.Mesh(this.geometry, this.material);
    el.setObject3D('mesh', this.mesh);

    // rotate
    this.mesh.rotation.x = this.data.thetaX;
    this.mesh.rotation.y = this.data.thetaY;
  },
})

AFRAME.registerComponent('s-orbital', {
    tick: function (t, dt) {
      var mesh = this.el.getObject3D('mesh');
      mesh.traverse(function (node) {
        // update node.position.<x/y/z>
      });      
    }
  });