from collections import defaultdict, OrderedDict
from pathlib import Path
from random import randint
from math import pi, sqrt

from flask import Blueprint, render_template, send_from_directory, redirect, url_for

this_dir = Path(__file__).parent
static_dir = this_dir.joinpath('static/dist').resolve()
templates_dir = this_dir.joinpath('static/').resolve()

aframe_bp = Blueprint('aframe', __name__, static_folder=str(static_dir), template_folder=str(templates_dir))


@aframe_bp.route('/')
def index():
    return redirect(url_for('aframe.bohr', num_electrons=6))


def num_electrons_to_shells(num_electrons):
    MAX_PER_SUBSHELL = OrderedDict([
        ('s', 2),
        ('p', 6),
        ('d', 10),
        ('f', 14),
        ('g', 18),
    ])
    ALLOWED_SUBSHELLS = OrderedDict([
        (1, ('s')),
        (2, ('s', 'p')),
        (3, ('s', 'p', 'd')),
        (4, ('s', 'p', 'd', 'f'))
    ])

    subshell = defaultdict(int)
    shells = defaultdict(subshell.copy)  # 1: {s:1} or 1: {s:2}, 2: {s:2, p:1} etc
    while num_electrons > 0:
        used_electrons = 0
        for shell_num, subshells in ALLOWED_SUBSHELLS.items():
            for subshell in subshells:
                num_in_subshell = shells[shell_num][subshell]
                max_allowed = MAX_PER_SUBSHELL[subshell]
                if num_in_subshell == max_allowed:
                    continue  # move out
                else:
                    shells[shell_num][subshell] += 1
                    used_electrons = 1
                    break
            if used_electrons:
                break
        num_electrons -= 1
        used_electrons = 0

    # figure out the number in each shell
    num_per_shell = OrderedDict()
    for shell_num, subshells in shells.items():
        total = sum(subshells.values())
        num_per_shell[shell_num] = total
    return shells, num_per_shell


@aframe_bp.route('/bohr/<int:num_electrons>')
def bohr(num_electrons):
    num_electrons = max([1, num_electrons])  # negative numbers not allowed
    bohr_radius = 0.5
    mass_to_base_energy_ratio = 1

    max_radius = bohr_radius
    electrons = []

    # figure out configuration
    _, num_per_shell = num_electrons_to_shells(num_electrons)
    print(num_per_shell)

    for shell_num, num_electrons_in_shell in num_per_shell.items():
        r = shell_num**2 * bohr_radius
        max_radius = max([max_radius, r])
        period = 2*pi*(shell_num**3)*bohr_radius*sqrt(mass_to_base_energy_ratio/2)

        thetaYDiff = 180 / (num_electrons_in_shell+1)
        thetaY = 0
        for idx in range(num_electrons_in_shell):
            thetaY += thetaYDiff
            electrons.append({
                'orbitalRadius': r,
                'orbitalPhase': randint(0, 179),
                'orbitsPerSec': 1/period,
                'thetaX': randint(0, 89),
                'thetaY': thetaY,
                'id': f'{shell_num}-e{idx+1}',
            })

    return render_template('bohr.html', electrons=electrons, max_radius=max_radius)


@aframe_bp.route('/atoms.js')
def atoms_js():
    return send_from_directory(templates_dir, 'atoms.js')


@aframe_bp.route('/_health')
def healthcheck():
    return 'aframe', 200
