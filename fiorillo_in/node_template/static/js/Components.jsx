import React from "react";

export default class Content extends React.Component {
  render() {
    return [
      <div class="jumbotron text-center">
        <h1>A Node-Bootstrap-React Boilerplate!</h1>
        <p class="lead">This example uses Bootstrap 3, Sass, React.js, and Webpack on the frontend. It's all served by a Flask server!</p>
      </div>,
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h4>React.js</h4>
            <p>This part came from within a React component.</p>

            <h4>Bootstrap</h4>
            <p>Mobile resizing is at least feasible!</p>
          </div>

          <div class="col-lg-6">
            <h4>Webpack</h4>
            <p>All of this stuff was bundled together by Webpack.</p>

            <h4>npm and Flask</h4>
            <p>This is basically a full-stack web app.</p>
          </div>
        </div>
      </div>
    ];
  }
}