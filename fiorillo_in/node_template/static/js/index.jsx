import React from "react";
import ReactDOM from "react-dom";

import Content from "./Components";

ReactDOM.render(
    <Content />,
    document.getElementById("content"));