from pathlib import Path

from flask import Blueprint, render_template

this_dir = Path(__file__).parent
static_dir = this_dir.joinpath('static/dist').resolve()
templates_dir = this_dir.joinpath('static/').resolve()

node_template_bp = Blueprint('node_template',
                             __name__,
                             static_folder=str(static_dir),
                             template_folder=str(templates_dir))


@node_template_bp.route('/')
def index():
    return render_template('index.jinja')


@node_template_bp.route('/_health')
def healthcheck():
    return 'node', 200
