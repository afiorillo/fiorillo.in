from flask import Flask

from fiorillo_in.config import current_config
from fiorillo_in.utils import healthcheck, send_to_blog, favicon
from fiorillo_in.germany import germany_bp
from fiorillo_in.poc import poc
# from fiorillo_in.node_template import node_template_bp
from fiorillo_in.aframe import aframe_bp


def make_app():
    app = Flask(__name__)
    app.config.from_object(current_config)
    register_endpoints(app)

    return app


def register_endpoints(app):
    app.add_url_rule('/', endpoint='index', view_func=send_to_blog)
    app.add_url_rule('/favicon.ico', endpoint='favicon', view_func=favicon)
    app.add_url_rule('/_health', endpoint='healthcheck', view_func=healthcheck)
    # register blueprints
    app.register_blueprint(germany_bp, url_prefix='/germany')
    app.register_blueprint(germany_bp, url_prefix='/deutschland')
    # app.register_blueprint(node_template_bp, url_prefix='/node-template')
    app.register_blueprint(aframe_bp, url_prefix='/aframe')
    app.register_blueprint(poc, url_prefix='/poc')


app = make_app()

if __name__ == '__main__':
    app.run(host='127.0.0.1')
