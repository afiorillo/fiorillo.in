from flask import Blueprint

germany_bp = Blueprint('germany', __name__)


@germany_bp.route('/_health')
def healthcheck():
    return 'alles gut', 200
