from pathlib import Path

from flask import Blueprint

this_dir = Path(__file__).parent
static_dir = this_dir.joinpath('www/').resolve()

poc = Blueprint('poc', __name__, static_folder=str(static_dir))


@poc.route('/')
def root():
    return poc.send_static_file('index.html')


@poc.route('/CKB51029g.gif')
def img1():
    return poc.send_static_file('CKB51029g.gif')


@poc.route("/CKB51078ag.gif")
def file0():
    return poc.send_static_file("CKB51078ag.gif")


@poc.route("/CKB51079ag.gif")
def file1():
    return poc.send_static_file("CKB51079ag.gif")


@poc.route("/CKB51080ag.gif")
def file2():
    return poc.send_static_file("CKB51080ag.gif")


@poc.route("/CKB51081ag.gif")
def file3():
    return poc.send_static_file("CKB51081ag.gif")


@poc.route("/CKB51082ag.gif")
def file4():
    return poc.send_static_file("CKB51082ag.gif")


@poc.route("/CKB51084ag.gif")
def file5():
    return poc.send_static_file("CKB51084ag.gif")


@poc.route("/CKB51492g.gif")
def file6():
    return poc.send_static_file("CKB51492g.gif")


@poc.route("/CKB51527g.gif")
def file7():
    return poc.send_static_file("CKB51527g.gif")


@poc.route("/c.htm")
def file8():
    return poc.send_static_file("c.htm")


@poc.route("/e.htm")
def file9():
    return poc.send_static_file("e.htm")


@poc.route("/f.htm")
def file10():
    return poc.send_static_file("f.htm")


@poc.route("/index.htm")
def file11():
    return poc.send_static_file("index.htm")


@poc.route("/index.html")
def file12():
    return poc.send_static_file("index.html")


@poc.route("/j.htm")
def file13():
    return poc.send_static_file("j.htm")


@poc.route("/ja.htm")
def file14():
    return poc.send_static_file("ja.htm")


@poc.route("/l.htm")
def file15():
    return poc.send_static_file("l.htm")


@poc.route("/ll.htm")
def file16():
    return poc.send_static_file("ll.htm")


@poc.route("/lt.htm")
def file17():
    return poc.send_static_file("lt.htm")


@poc.route("/lw.htm")
def file18():
    return poc.send_static_file("lw.htm")


@poc.route("/ps.htm")
def file19():
    return poc.send_static_file("ps.htm")


@poc.route("/t.htm")
def file20():
    return poc.send_static_file("t.htm")


@poc.route("/w.htm")
def file21():
    return poc.send_static_file("w.htm")
