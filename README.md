fiorillo.in
---

The GAE project hosted at [https://fiorillo.in/](https://fiorillo.in/).


# Development


## Python Setup

This project uses [`pipenv`](https://pipenv.readthedocs.io/en/latest/) to manage packages.
To setup the development environment

```bash
$ pipenv install --dev
```

## GAE Setup

This project is intended to be hosted on Google AppEngine.
To deploy, you must have the [Google Cloud SDK](https://cloud.google.com/sdk/docs/) installed.
On Arch Linux distributions, you can use the `google-cloud-sdk` package from the AUR:

```bash
$ yay -S google-cloud-sdk
```
