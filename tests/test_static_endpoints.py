import unittest

from fiorillo_in.app import app as main_app


class TestStaticEndpoints(unittest.TestCase):

    def setUp(self):
        main_app.testing = True
        self.app = main_app.test_client()

    def test_index(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code, 302, 'index is not a redirect')

    def test_healthcheck(self):
        result = self.app.get('/_health')
        self.assertEqual(result.status_code, 200, 'main healthcheck failed')

    def test_germany_healthcheck(self):
        result = self.app.get('/germany/_health')
        self.assertEqual(result.status_code, 200, 'germany healthcheck failed')

    def test_deutschland_healthcheck(self):
        result = self.app.get('/deutschland/_health')
        self.assertEqual(result.status_code, 200, 'deutschland healthcheck failed')
